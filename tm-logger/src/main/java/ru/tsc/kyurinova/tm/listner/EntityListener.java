package ru.tsc.kyurinova.tm.listner;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.kyurinova.tm.service.LoggerService;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

@Component
public class EntityListener implements MessageListener {

    @Autowired
    @NotNull
    private LoggerService loggerService;

    @Override
    @SneakyThrows
    public void onMessage(Message message) {
        final boolean checkType = message instanceof TextMessage;
        if (!checkType) return;
        @NotNull final TextMessage textMessage = (TextMessage) message;
        @NotNull final String text = textMessage.getText();
        loggerService.log(text);
    }

}
