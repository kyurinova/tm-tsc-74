package ru.tsc.kyurinova.tm.unit.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import ru.tsc.kyurinova.tm.api.service.dto.IProjectDTOService;
import ru.tsc.kyurinova.tm.dto.model.ProjectDTO;
import ru.tsc.kyurinova.tm.enumerated.Status;
import ru.tsc.kyurinova.tm.marker.WebUnitCategory;
import ru.tsc.kyurinova.tm.util.UserUtil;

import java.util.List;

import static ru.tsc.kyurinova.tm.constant.ProjectTestData.*;

@SpringBootTest
@AutoConfigureMockMvc
@Category(WebUnitCategory.class)
public class ProjectServiceTest {

    @NotNull
    private final ProjectDTO project1 = new ProjectDTO(PROJECT1_NAME, PROJECT1_DESCRIPTION);

    @NotNull
    private final ProjectDTO project2 = new ProjectDTO(PROJECT2_NAME, PROJECT2_DESCRIPTION);

    @NotNull
    private final ProjectDTO project3 = new ProjectDTO(PROJECT3_NAME, PROJECT3_DESCRIPTION);

    @NotNull
    @Autowired
    private IProjectDTOService projectService;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @Nullable
    private String userId;

    @Before
    @SneakyThrows
    public void before() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(USER_NAME, USER_PASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        userId = UserUtil.getUserId();
        project1.setUserId(userId);
        project2.setUserId(userId);
        projectService.add(project1);
        projectService.add(project2);
    }

    @After
    @SneakyThrows
    public void after() {
        projectService.clear();
    }

    @Test
    @SneakyThrows
    public void addTest() {
        projectService.add(project3);
        Assert.assertNotNull(projectService.findOneById(project3.getId()));
    }

    @Test
    @SneakyThrows
    public void addByUserIdTest() {
        projectService.addByUserId(userId, project3);
        Assert.assertNotNull(projectService.findOneByUserIdAndId(userId, project3.getId()));
    }

    @Test
    @SneakyThrows
    public void changeProjectStatusByIdTest() {
        projectService.changeProjectStatusById(project1.getId(), Status.IN_PROGRESS);
        @Nullable final ProjectDTO project = projectService.findOneById(project1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getStatus(), Status.IN_PROGRESS);
    }

    @Test
    @SneakyThrows
    public void changeProjectStatusByUserIdAndIdTest() {
        projectService.changeProjectStatusByUserIdAndId(userId, project1.getId(), Status.IN_PROGRESS);
        @Nullable final ProjectDTO project = projectService.findOneByUserIdAndId(userId, project1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getStatus(), Status.IN_PROGRESS);
    }

    @Test
    @SneakyThrows
    public void clearTest() {
        projectService.clear();
        Assert.assertEquals(0, projectService.count());
    }

    @Test
    @SneakyThrows
    public void clearByUserIdTest() {
        projectService.clearByUserId(userId);
        Assert.assertEquals(0, projectService.countByUserId(userId));
    }

    @Test
    @SneakyThrows
    public void countTest() {
        Assert.assertEquals(2, projectService.count());
    }

    @Test
    @SneakyThrows
    public void countByUserIdTest() {
        Assert.assertEquals(2, projectService.countByUserId(userId));
    }

    @Test
    @SneakyThrows
    public void createTest() {
        @Nullable final ProjectDTO project = projectService.create(userId, PROJECT3_NAME);
        Assert.assertNotNull(projectService.findOneByUserIdAndId(userId, project.getId()));
    }

    @Test
    @SneakyThrows
    public void createWithDescriptionTest() {
        @Nullable final ProjectDTO project = projectService.create(userId, PROJECT3_NAME, PROJECT3_DESCRIPTION);
        Assert.assertNotNull(projectService.findOneByUserIdAndId(userId, project.getId()));
    }

    @Test
    @SneakyThrows
    public void existsByIdTest() {
        Assert.assertFalse(projectService.existsById(""));
        Assert.assertTrue(projectService.existsById(project1.getId()));
    }

    @Test
    @SneakyThrows
    public void existsByUserIdAndIdTest() {
        Assert.assertFalse(projectService.existsByUserIdAndId(userId, ""));
        Assert.assertTrue(projectService.existsByUserIdAndId(userId, project1.getId()));
    }

    @Test
    @SneakyThrows
    public void findAllTest() {
        @Nullable final List<ProjectDTO> projects = projectService.findAll();
        Assert.assertNotNull(projects);
        Assert.assertEquals(2, projects.size());
    }

    @Test
    @SneakyThrows
    public void findAllByUserIdTest() {
        @Nullable final List<ProjectDTO> projects = projectService.findAllByUserId(userId);
        Assert.assertNotNull(projects);
        Assert.assertEquals(2, projects.size());
    }

    @Test
    @SneakyThrows
    public void findOneByIdTest() {
        @Nullable final ProjectDTO project = projectService.findOneById(project1.getId());
        Assert.assertNotNull(project);
    }

    @Test
    @SneakyThrows
    public void findOneByUserIdAndIdTest() {
        @Nullable final ProjectDTO project = projectService.findOneByUserIdAndId(userId, project1.getId());
        Assert.assertNotNull(project);
    }

    @Test
    @SneakyThrows
    public void removeTest() {
        projectService.remove(project1);
        Assert.assertNull(projectService.findOneById(project1.getId()));
    }

    @Test
    @SneakyThrows
    public void removeByUserIdTest() {
        projectService.removeByUserId(userId, project1);
        Assert.assertNull(projectService.findOneByUserIdAndId(userId, project1.getId()));
    }

    @Test
    @SneakyThrows
    public void removeByIdTest() {
        projectService.removeById(project1.getId());
        Assert.assertNull(projectService.findOneById(project1.getId()));
    }

    @Test
    @SneakyThrows
    public void removeByUserIdAndIdTest() {
        projectService.removeByUserIdAndId(userId, project1.getId());
        Assert.assertNull(projectService.findOneByUserIdAndId(userId, project1.getId()));
    }

    @Test
    @SneakyThrows
    public void updateTest() {
        project1.setStatus(Status.IN_PROGRESS);
        @Nullable final ProjectDTO project = projectService.update(project1);
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getStatus(), Status.IN_PROGRESS);
    }

    @Test
    @SneakyThrows
    public void updateByUserIdTest() {
        project1.setStatus(Status.IN_PROGRESS);
        @Nullable final ProjectDTO project = projectService.updateByUserId(userId, project1);
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getStatus(), Status.IN_PROGRESS);
    }

    @Test
    @SneakyThrows
    public void updateByIdTest() {
        @Nullable final ProjectDTO project = projectService.updateById(project1.getId(), PROJECT3_NAME, PROJECT3_DESCRIPTION);
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getName(), PROJECT3_NAME);
        Assert.assertEquals(project.getDescription(), PROJECT3_DESCRIPTION);
    }

    @Test
    @SneakyThrows
    public void updateByUserIdAndIdTest() {
        @Nullable final ProjectDTO project = projectService.updateByUserIdAndId(userId, project1.getId(), PROJECT3_NAME, PROJECT3_DESCRIPTION);
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getName(), PROJECT3_NAME);
        Assert.assertEquals(project.getDescription(), PROJECT3_DESCRIPTION);
    }

}
