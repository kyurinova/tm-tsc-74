package ru.tsc.kyurinova.tm.integration.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import ru.tsc.kyurinova.tm.dto.model.TaskDTO;
import ru.tsc.kyurinova.tm.dto.model.UserDTO;
import ru.tsc.kyurinova.tm.enumerated.Status;
import ru.tsc.kyurinova.tm.marker.WebIntegrationCategory;
import ru.tsc.kyurinova.tm.model.Result;

import java.net.HttpCookie;
import java.util.Arrays;
import java.util.List;

import static ru.tsc.kyurinova.tm.constant.TaskTestData.*;

@Category(WebIntegrationCategory.class)
public class TasksRestEndpointTest {

    @NotNull
    private static final String BASE_URL = "http://localhost:8080/api/tasks/";
    @NotNull
    private static final HttpHeaders header = new HttpHeaders();
    @Nullable
    private static String sessionId;
    @Nullable
    private static String userId;
    @NotNull
    private final TaskDTO task1 = new TaskDTO(TASK1_NAME, TASK1_DESCRIPTION);
    @NotNull
    private final TaskDTO task2 = new TaskDTO(TASK2_NAME, TASK2_DESCRIPTION);
    @NotNull
    private final TaskDTO task3 = new TaskDTO(TASK3_NAME, TASK3_DESCRIPTION);

    @BeforeClass
    public static void beforeClass() throws JsonProcessingException {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = "http://localhost:8080/api/auth/login?username=test&password=test";
        header.setContentType(MediaType.APPLICATION_JSON);
        @NotNull final ResponseEntity<Result> response = restTemplate.postForEntity(url, new HttpEntity<>(""), Result.class);
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().getSuccess());
        @NotNull final HttpHeaders headersResponse = response.getHeaders();
        List<HttpCookie> cookies = HttpCookie.parse(headersResponse.getFirst(HttpHeaders.SET_COOKIE));
        sessionId = cookies.stream()
                .filter(item -> "JSESSIONID".equals(item.getName())
                ).findFirst().get().getValue();
        Assert.assertNotNull(sessionId);
        header.put(HttpHeaders.COOKIE, Arrays.asList("JSESSIONID=" + sessionId));
        @NotNull final String urlProfile = "http://localhost:8080/api/auth/profile";
        @NotNull final ResponseEntity<UserDTO> responseProfile = restTemplate.exchange(urlProfile, HttpMethod.GET, new HttpEntity<>(header), UserDTO.class);
        userId = responseProfile.getBody().getId();
    }

    private static ResponseEntity<List> sendRequestList(@NotNull final String url, @NotNull final HttpMethod method, @NotNull final HttpEntity<List> httpEntity) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, List.class);
    }

    private static ResponseEntity<TaskDTO> sendRequest(@NotNull final String url, @NotNull final HttpMethod method, @NotNull final HttpEntity<TaskDTO> httpEntity) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, TaskDTO.class);
    }

    @AfterClass
    public static void afterClass() {
        @NotNull final String url = "http://localhost:8080/api/auth/logout";
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(header));
    }

    @Before
    public void before() {
        @NotNull final String url = BASE_URL + "add/";
        task1.setUserId(userId);
        task2.setUserId(userId);
        task3.setUserId(userId);
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(task1, header));
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(task2, header));
    }

    @After
    public void after() {
        @NotNull final String url = BASE_URL + "deleteAll/";
        sendRequestList(url, HttpMethod.POST, new HttpEntity<>(header));
    }

    @Test
    public void findAllTest() throws Exception {
        @NotNull final String url = BASE_URL + "findAll/";
        Assert.assertEquals(2, sendRequestList(url, HttpMethod.GET, new HttpEntity<>(header)).getBody().size());
    }

    @Test
    public void addTest() throws Exception {
        @NotNull final String url = BASE_URL + "add/";
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(task3, header));
        @NotNull final String findUrl = BASE_URL + "findById/" + task3.getId();
        Assert.assertNotNull(sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(header)).getBody());
    }

    @Test
    public void saveTest() throws Exception {
        @NotNull final String url = BASE_URL + "save/";
        task1.setStatus(Status.IN_PROGRESS);
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(task1, header));
        @NotNull final String findUrl = BASE_URL + "findById/" + task1.getId();
        Assert.assertEquals(Status.IN_PROGRESS, sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(header)).getBody().getStatus());
    }

    @Test
    public void findByIdTest() throws Exception {
        @NotNull final String url = BASE_URL + "findById/" + task1.getId();
        Assert.assertNotNull(sendRequest(url, HttpMethod.GET, new HttpEntity<>(header)).getBody());
    }

    @Test
    public void deleteTest() throws Exception {
        @NotNull final String url = BASE_URL + "delete/";
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(task1, header));
        @NotNull final String findUrl = BASE_URL + "findAll/";
        Assert.assertEquals(1, sendRequestList(findUrl, HttpMethod.GET, new HttpEntity<>(header)).getBody().size());
    }

    @Test
    public void deleteAllTest() throws Exception {
        @NotNull final String url = BASE_URL + "deleteAll/";
        sendRequestList(url, HttpMethod.POST, new HttpEntity<>(header));
        @NotNull final String findUrl = BASE_URL + "findAll/";
        Assert.assertEquals(0, sendRequestList(findUrl, HttpMethod.GET, new HttpEntity<>(header)).getBody().size());
    }

}