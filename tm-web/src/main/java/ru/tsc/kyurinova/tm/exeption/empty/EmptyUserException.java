package ru.tsc.kyurinova.tm.exeption.empty;

import ru.tsc.kyurinova.tm.exeption.AbstractException;

public class EmptyUserException extends AbstractException {

    public EmptyUserException() {
        super("Error. User not found.");
    }

}
