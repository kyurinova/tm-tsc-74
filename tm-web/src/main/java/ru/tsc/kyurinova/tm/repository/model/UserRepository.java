package ru.tsc.kyurinova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.tsc.kyurinova.tm.model.User;

@Repository
@Scope("prototype")
public interface UserRepository extends JpaRepository<User, String> {

    @Nullable
    User findByLogin(@NotNull final String login);

}
