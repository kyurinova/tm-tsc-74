package ru.tsc.kyurinova.tm.service.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.kyurinova.tm.api.service.model.IService;
import ru.tsc.kyurinova.tm.model.AbstractModel;
import ru.tsc.kyurinova.tm.repository.model.AbstractRepository;

import javax.transaction.Transactional;
import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractService<M extends AbstractModel> implements IService<M> {

    @NotNull
    @Autowired
    private AbstractRepository<M> repository;

    @Transactional
    public void clear() {
        repository.deleteAll();
    }

    public List<M> findAll() {
        return repository.findAll();
    }

}
