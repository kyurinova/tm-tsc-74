package ru.tsc.kyurinova.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Controller;
import ru.tsc.kyurinova.tm.api.endpoint.ISessionEndpoint;
import ru.tsc.kyurinova.tm.dto.model.SessionDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@Controller
public class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull
    SessionDTO openSession(
            @NotNull
            @WebParam(name = "login", partName = "login")
                    String login,
            @NotNull
            @WebParam(name = "password", partName = "password")
                    String password
    ) {
        return sessionService.open(login, password);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void closeSession(
            @NotNull
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    ) {
        sessionService.close(session);
    }

}
