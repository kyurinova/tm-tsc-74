package ru.tsc.kyurinova.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.tsc.kyurinova.tm.api.endpoint.IAdminDataEndpoint;
import ru.tsc.kyurinova.tm.api.service.IAdminDataService;
import ru.tsc.kyurinova.tm.dto.Domain;
import ru.tsc.kyurinova.tm.enumerated.Role;
import ru.tsc.kyurinova.tm.dto.model.SessionDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@Controller
public class AdminDataEndpoint extends AbstractEndpoint implements IAdminDataEndpoint {

    @NotNull
    @Autowired
    private IAdminDataService adminDataService;

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull
    Domain getDomain(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    ) {
        sessionService.validate(session, Role.ADMIN);
        return adminDataService.getDomain();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void setDomain(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "domain", partName = "domain")
                    Domain domain) {
        sessionService.validate(session, Role.ADMIN);
        adminDataService.setDomain(domain);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void dataBackupLoad(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    ) {
        sessionService.validate(session, Role.ADMIN);
        adminDataService.dataBackupLoad();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void dataBackupSave(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    ) {
        sessionService.validate(session, Role.ADMIN);
        adminDataService.dataBackupSave();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void dataBase64Load(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    ) {
        sessionService.validate(session, Role.ADMIN);
        adminDataService.dataBase64Load();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void dataBase64Save(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    ) {
        sessionService.validate(session, Role.ADMIN);
        adminDataService.dataBase64Save();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void dataBinaryLoad(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    ) {
        sessionService.validate(session, Role.ADMIN);
        adminDataService.dataBinaryLoad();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void dataBinarySave(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    ) {
        sessionService.validate(session, Role.ADMIN);
        adminDataService.dataBinarySave();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void dataJsonLoadFasterXML(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    ) {
        sessionService.validate(session, Role.ADMIN);
        adminDataService.dataJsonLoadFasterXML();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void dataJsonLoadJaxB(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    ) {
        sessionService.validate(session, Role.ADMIN);
        adminDataService.dataJsonLoadJaxB();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void dataJsonSaveFasterXML(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    ) {
        sessionService.validate(session, Role.ADMIN);
        adminDataService.dataJsonSaveFasterXML();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void dataJsonSaveJaxB(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    ) {
        sessionService.validate(session, Role.ADMIN);
        adminDataService.dataJsonSaveJaxB();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void dataXmlLoadFasterXML(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    ) {
        sessionService.validate(session, Role.ADMIN);
        adminDataService.dataXmlLoadFasterXML();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void dataXmlLoadJaxBC(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    ) {
        sessionService.validate(session, Role.ADMIN);
        adminDataService.dataXmlLoadJaxBC();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void dataXmlSaveFasterXML(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    ) {
        sessionService.validate(session, Role.ADMIN);
        adminDataService.dataXmlSaveFasterXML();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void dataXmlSaveJaxB(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    ) {
        sessionService.validate(session, Role.ADMIN);
        adminDataService.dataXmlSaveJaxB();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void dataYamlLoadFasterXML(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    ) {
        sessionService.validate(session, Role.ADMIN);
        adminDataService.dataYamlLoadFasterXML();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void dataYamlSaveFasterXML(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    ) {
        sessionService.validate(session, Role.ADMIN);
        adminDataService.dataYamlSaveFasterXML();
    }
}
